var NewPerson = function(name, salary){
	this.name = name;
	this.salary = salary;
}

NewPerson.prototype = {
	work : function (payment){
		var decision = (payment >= this.salary);
		return decision;
	},

	sayHello : function (){
		return console.log('hello! I am ' + this.name);
    }

}