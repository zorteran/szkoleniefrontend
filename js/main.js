
//windows.addEventListener("load",function(event){})
document.addEventListener('DOMContentLoaded',
    function () {
        console.log('app start!');
        var playlistTable = $(".playlist_table");
        var playlistRows = playlistTable.find("tbody");
        var form = $('.playlist_form');
        var submit = form.find('submit,button[type=submit]');
        var fields = form.get(0).elements;

        function makeRow(data){
            return $( '<tr data-id="'+data.i+'">\
                <td>'+data.title+'</td>\
                <td>'+data.author+'</td>\
                <td><button class=\'btn btn-success\'>Play</button></td>\
                </tr>'
            )
        }

        function onSubmit(event){
            var data = {
                i: playlistRows.children().lenght+1,
                title: fields.title.value,
                author: fields.author.value
            }
            var tr = makeRow(data);
            playlistRows.append(tr);
            event.preventDefault();
            fields.title.value = "";
            fields.author.value = "";
            fields.title.focus();
        }

        submit.on('click', onSubmit )


    }
)

